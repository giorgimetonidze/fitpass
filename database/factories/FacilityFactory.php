<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FacilityFactory extends Factory
{
    protected $suffixes = ["Gym","Fitness"];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => Str::uuid(),
            'name' => $this->generateFacilityName()
        ];
    }

    protected function generateFacilityName() {
        return $this->faker->name() .' '. $this->suffixes[array_rand($this->suffixes)];
    }
}
