<?php

use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;
use Illuminate\Support\Facades\Route;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//first task
Route::get('/set', function () {
    $factory = new RandomLib\Factory;
//    $generator = $factory->getLowStrengthGenerator();
    $generator = $factory->getMediumStrengthGenerator();
//    $bytes = $generator->generate(32);
    $randomString = $generator->generateString(6, '!#$%&(){}[]=');
     dd($randomString);
});


//second task
//Strength 1
Route::get('/password1', function () {
    $generator = new RequirementPasswordGenerator();
    $generator
        ->setLength(6)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_UPPER_CASE, true)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_LOWER_CASE, true)
//        ->setOptionValue(RequirementPasswordGenerator::OPTION_NUMBERS, true)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, 2)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, 1)
//        ->setMaximumCount(RequirementPasswordGenerator::OPTION_NUMBERS, 6)
        ->setMaximumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, 6)
    ;
    $password = $generator->generatePassword();
    dd($password);
});
//Strength 2
Route::get('/password2', function () {
    $generator = new RequirementPasswordGenerator();
    $generator
        ->setLength(6)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_UPPER_CASE, true)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_LOWER_CASE, true)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_NUMBERS, true)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, 2)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, 1)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_NUMBERS, 1)
        ->setMaximumCount(RequirementPasswordGenerator::OPTION_NUMBERS, 6)
        ->setMaximumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, 6)
    ;
    $password = $generator->generatePassword();
    dd($password);
});

//Strength 3
Route::get('/password3', function () {
    $generator = new RequirementPasswordGenerator();
    $generator
        ->setLength(6)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_UPPER_CASE, true)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_LOWER_CASE, true)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_NUMBERS, true)
        ->setOptionValue(RequirementPasswordGenerator::OPTION_SYMBOLS, true)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, 2)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, 2)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_NUMBERS, 1)
        ->setMinimumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, 1)
        ->setMaximumCount(RequirementPasswordGenerator::OPTION_NUMBERS, 6)
        ->setMaximumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, 6)
    ;
    $password = $generator->generatePassword();
    dd($password);
});


