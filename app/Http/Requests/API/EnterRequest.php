<?php

namespace App\Http\Requests\API;

use App\Models\UserEntry;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class EnterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $startDay = Carbon::now()->startOfDay();
        $endDay   = $startDay->copy()->endOfDay();
        if (empty(UserEntry::where('card_uuid',request()->input('card_uuid'))
            ->whereBetween('created_at',[$startDay,$endDay])
            ->first()
        )) return true;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'object_uuid' => 'required|alpha_dash',
            'card_uuid' => 'required|alpha_dash'
        ];
    }
}
