<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\EnterRequest;
use App\Models\Card;
use App\Models\Facility;
use App\Models\UserEntry;
use Illuminate\Http\Request;

class EntranceController extends Controller
{
    public function enter(EnterRequest $request)
    {

        $card = Card::with('user')->where("uuid", $request->input('card_uuid'))->first();
        $facility = Facility::where("uuid", $request->input('object_uuid'))->first();
        if (!empty($card) && !empty($facility)) {
            UserEntry::create([
                'card_uuid' => $card->uuid,
                'object_uuid' => $facility->uuid,
            ]);

            return response([
                'status' => 'OK',
                'object_name' => $facility->name,
                'first_name' => $card->user->first_name,
                'last_name' => $card->user->last_name,
            ], 200);
        } else {
            return response("Card uuid or Facility uuid is incorrect",404);
        }
    }
}
